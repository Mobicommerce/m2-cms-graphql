<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_CmsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\CmsGraphQl\Plugin\Magento\Cms\Model\Page;

class IdentityMap
{
    public function __construct(
        \Magento\Cms\Helper\Page $pageHelper,
        \Mobicommerce\Mobiapp\Helper\CmsPage $mobiCmsPageHelper
    ) {
        $this->pageHelper = $pageHelper;
        $this->mobiCmsPageHelper = $mobiCmsPageHelper;
    }

    public function beforeAdd($subject, $page)
    {
        if ($page->getId()) {
            $page->setData(
                'mobicommerce_seo_widget_ids',
                $this->mobiCmsPageHelper->getMobicommerceSeoWidgets($page->getId())
            );
        }
        return [$page];
    }
}
