<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_CmsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\CmsGraphQl\Plugin\Magento\CmsGraphQl\Model\Resolver\DataProvider;

use Magento\Cms\Api\PageRepositoryInterface;

class Page
{
    public function __construct(
        PageRepositoryInterface $pageRepository,
        \Magento\Cms\Helper\Page $pageHelper,
        \Mobicommerce\Mobiapp\Helper\CmsPage $mobiCmsPageHelper
    ) {
        $this->pageRepository = $pageRepository;
        $this->pageHelper = $pageHelper;
        $this->mobiCmsPageHelper = $mobiCmsPageHelper;
    }

    public function afterGetDataByPageId($subject, $result, $pageId)
    {
        $result['url_key'] = $this->pageHelper->getPageUrl($pageId);

        $page = $this->pageRepository->getById($pageId);
        foreach ([
            'mobicommerce_page_type',
            'mobicommerce_pwa_landing_page_id',
            'mobicommerce_desktop_landing_page_id',
            'mobicommerce_mobileapp_landing_page_id',
            'mobicommerce_product_collection_id'
        ] as $field) {
            $result[$field] = $page->getData($field);
        }

        $result['mobicommerce_seo_widget_ids'] = $this->mobiCmsPageHelper->getMobicommerceSeoWidgets($pageId);
        return $result;
    }
}
