<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_CmsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\CmsGraphQl\Model\Resolver\Pages;

use Magento\Framework\GraphQl\Query\Resolver\IdentityInterface;
use Magento\Cms\Model\Page;

/**
 * Identity for resolved PageBuilder
 */
class Identity implements IdentityInterface
{
    /**
     * Get PageBuilder ID from resolved data
     *
     * @param array $resolvedData
     * @return string[]
     */
    public function getIdentities(array $resolvedData): array
    {
        $pages = $resolvedData;

        if ($pages) {
            foreach ($pages as $page) {
                $ids[] = sprintf('%s_%s', Page::CACHE_TAG, $page['page_id']);
            }
        }

        $ids = array_unique($ids);

        return $ids;
    }
}
