<?php
/**
 * Mobicommerce
 * Copyright (C) 2021 Mobicommerce <info@mobicommerce.net>
 *
 * @category Mobicommerce
 * @package Mobicommerce_CmsGraphQl
 * @copyright Copyright (c) 2021 Mobicommerce (http://www.mobicommerce.net/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Mobicommerce <info@mobicommerce.net>
 */

namespace Mobicommerce\CmsGraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory;

class Pages implements ResolverInterface
{
    public function __construct(
        CollectionFactory $collectionFactory,
        \Magento\Cms\Helper\Page $pageHelper,
        \Mobicommerce\Mobiapp\Helper\CmsPage $mobiCmsPageHelper
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->pageHelper = $pageHelper;
        $this->mobiCmsPageHelper = $mobiCmsPageHelper;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $data = [];

        try {
            $collection = $this->collectionFactory->create();
            $this->mobiCmsPageHelper->getMobicommerceSeoWidgetsCollection($collection);
            
            $data = $collection->getData();

            if ($data) {
                foreach ($data as &$item) {
                    $item['url_key'] = $this->pageHelper->getPageUrl($item['page_id']);
                }
            }
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $data;
    }
}
